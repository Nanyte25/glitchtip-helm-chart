# GlitchTip Helm Chart

We use this chart internally. However it's not fully documented yet nor tested in a wide range of scenarios.
If you are a helm and kubernetes expert - feel free to use this and help contribute to this repo.  

Based on https://gitlab.com/burke-software/django-helm-chart/

# Usage

1. Add our Helm chart repo `helm repo add glitchtip https://glitchtip.gitlab.io/glitchtip-helm-chart/`
2. Review our values.yaml. At a minimum you'll need to set env.secret.DATABASE_URL and env.secret.SECRET_KEY.
3. Install the chart `helm install glitchtip/glitchtip`

The default values assume a externally managed PostgresSQL database and a chart managed Redis. Redis in GlitchTip is ephemeral.

# Tips

- Use [helm diff](https://github.com/databus23/helm-diff) to preview changes
- We don't version this chart at this time
- Set image.tag to the GlitchTip version you wish to install
- Set redis.auth.password to avoid redis being entirely remade on each release
- If updating the chart, carefully review values for any new defaults
